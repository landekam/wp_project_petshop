-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2019 at 04:29 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `petshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `administratori`
--

CREATE TABLE `administratori` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(50) COLLATE utf8_croatian_ci NOT NULL,
  `zaporka` varchar(50) COLLATE utf8_croatian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `administratori`
--

INSERT INTO `administratori` (`id`, `username`, `zaporka`) VALUES
(1, 'admin1', 'petshop');

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

CREATE TABLE `korisnik` (
  `id` varchar(50) COLLATE utf8_croatian_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_croatian_ci NOT NULL,
  `zaporka` varchar(50) COLLATE utf8_croatian_ci NOT NULL,
  `salt` varchar(50) COLLATE utf8_croatian_ci NOT NULL,
  `ime` varchar(50) COLLATE utf8_croatian_ci NOT NULL,
  `prezime` varchar(50) COLLATE utf8_croatian_ci NOT NULL,
  `telefon` varchar(50) COLLATE utf8_croatian_ci DEFAULT NULL,
  `grad` varchar(50) COLLATE utf8_croatian_ci DEFAULT NULL,
  `adresa` varchar(50) COLLATE utf8_croatian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `korisnik`
--

INSERT INTO `korisnik` (`id`, `email`, `zaporka`, `salt`, `ime`, `prezime`, `telefon`, `grad`, `adresa`) VALUES
('5d245c2d65c75', 'test@mail.com', 'cc35cf2257917f11b238913e1cd37ebf', '736f324209cdd79e8cdc5d1ba85af609', 'Test', 'Test', '0123456789', 'Tester', 'Testerova 1b'),
('5d24acb4bf67a', 'chicha@gmail.com', '3ea554e7ae4471a56adaedebab31d6ff', 'c588b26e075bbc7b17c56436f02d221b', 'ÄŒiÄ‡a', 'ÄŒiko', '3', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `kosarica_artikli`
--

CREATE TABLE `kosarica_artikli` (
  `id` varchar(50) COLLATE utf8_croatian_ci NOT NULL,
  `korisnik_id` varchar(50) COLLATE utf8_croatian_ci NOT NULL,
  `proizvod_id` varchar(50) COLLATE utf8_croatian_ci NOT NULL,
  `kolicina` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `kosarica_artikli`
--

INSERT INTO `kosarica_artikli` (`id`, `korisnik_id`, `proizvod_id`, `kolicina`) VALUES
('5d269e2be5cd5', '5d245c2d65c75', '5d269ce0d3025', 2),
('5d269e3accc34', '5d245c2d65c75', '5d269d141884c', 1);

-- --------------------------------------------------------

--
-- Table structure for table `proizvodi`
--

CREATE TABLE `proizvodi` (
  `id` varchar(50) COLLATE utf8_croatian_ci NOT NULL,
  `ime` varchar(50) COLLATE utf8_croatian_ci NOT NULL,
  `cijena` decimal(5,2) NOT NULL,
  `opis` text COLLATE utf8_croatian_ci NOT NULL,
  `kategorija` varchar(20) COLLATE utf8_croatian_ci NOT NULL,
  `naslovna` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `proizvodi`
--

INSERT INTO `proizvodi` (`id`, `ime`, `cijena`, `opis`, `kategorija`, `naslovna`) VALUES
('5d269548df9a3', 'Zdjelica od nehrÄ‘ajuÄ‡eg Äelika', '29.00', '', 'psi', b'1'),
('5d26959397fa5', 'ÄŒetka dvostrana drvena metal/Äekinja', '29.50', 'Dvostruka Äetka s udobnom drvenom ruÄkom, prirodne boje.\r\n\r\nJedna strana ima metalne presvuÄene zupce kako bi zaÅ¡titili njeÅ¾nu koÅ¾u. Na drugoj strani su sintetiÄka vlakna.\r\nPosebno je pogodna za sve pse s vunastom podlakom koja ima tendenciju petljanja, kao i za sve koji trebaju redovito Äetkanje.\r\n\r\nMetalni zupci sluÅ¾e raspetljavanju Ävorova na podlaci, dok Äekinjama isÄeÅ¡ljavamo praÅ¡inu i ÄeÅ¡ljamo glatku dlaku.', 'psi', b'1'),
('5d2695d6ae65d', 'Vodilica na regulaciju', '99.00', '', 'psi', b'0'),
('5d26962bafad8', 'ALL FOR PAWS Pups Krevet, 36x49x13,5cm, tirkizni', '169.00', '', 'psi', b'0'),
('5d26967201468', 'Transporter plastiÄni', '389.00', 'Transporteri Nomad za srednje i velike pasmine.\r\nMetalna vrata s dvostrukom kontrolnom sigurnosnom bravom.\r\nIma pretinac za dokumente (jedan kod C190 / 4 verzije, dva kod C190 / 5 i C190 / 6 verzije).\r\nOdgovara IATA zahtjevima za prijevoz Å¾ivih Å¾ivotinja.\r\nUvijek zatraÅ¾ite dodatne informacije od avio kompanije prije nego namjeravate slati Å¾ive Å¾ivotinje. ', 'psi', b'0'),
('5d2696a331573', ' CAMON IgraÄka za pse Lopta uÅ¾e 30cm, razne boje', '14.90', '', 'psi', b'0'),
('5d269702aea67', 'REMI Janjetina i krumpir 12kg', '129.90', 'REMI Potpuna hrana za odrasle pse, s janjetinom i krumpirom\r\n\r\nREMI receptura s paÅ¾ljivo odabranim kvalitetnim sastojcima potpun je i lakoprobavljiv dnevni obrok, izvrstan za svakog psa.\r\n\r\nSastav: dehidrirano meso, kukuruz, pÅ¡enica, preraÄ‘evine biljnog podrijetla, dehidrirana janjetina (5%), krumpirov Å¡krob (4%), pileÄ‡a mast, minerali.\r\n\r\nAnalitiÄki sastav: bjelanÄevine 23%, sadrÅ¾aj masti 9%, sirova vlaknina 3,5%, sirovi pepeo 10%.\r\nDodaci: nutritivni dodaci/kg: vitamin A 10000 IJ, vitamin D3 750 IJ, vitamin E 70 mg, Å¾eljezo (Å¾eljezov karbonat) 80 mg, Å¾eljezo (Å¾eljezov oksid) 340 mg, cink (cinkov oksid) 100 mg, bakar (bakrov sulfat pentahidrat) 3,5 mg, mangan (manganov oksid) 8 mg, jod (kalcijev jodat) 1,7 mg; antioksidansi.\r\n\r\nUputa za koriÅ¡tenje i Äuvanje: Obrok moÅ¾ete posluÅ¾iti u suhom obliku ili po potrebi omekÅ¡ati s malo mlake vode ili juhe.\r\nPreporuÄeni dnevni unos hrane:\r\n(tjelesna masa psa u kilogramima = dnevna koliÄina proizvoda u gramima).\r\n3-5kg = 85-125g\r\n6-12kg = 125-250g\r\n13-20kg = 250-380g\r\n21-30kg = 380-510g\r\n31-40kg = 600-750g\r\n41-50kg = 750-900g\r\n\r\nKoliÄine su indikativne i mogu se prilagoditi specifi Änim potrebama\r\nsvakog psa. SvjeÅ¾a pitka voda mora uvijek biti dostupna.\r\nProizvod Äuvajte na suhom,\r\ntamnom i zraÄnom mjestu, pri sobnoj temperaturi.', 'psi', b'0'),
('5d269736323b0', 'Poslastica za psa Lamb Snack s janjetinom, 35g', '8.95', 'ALPHA SPIRIT Poslastica za psa Lamb Snack s janjetinom, dodatak prehrani za pse\r\n\r\nUkusne, lako probavljive, polu-vlaÅ¾ne poslastice, na koje je vrlo mala moguÄ‡nost pojave alergijske reakcije.\r\nZa zdravu i sjajnu dlaku.\r\nBogate prirodnim Å¾eljezom, te uÄinkovito Å¡tite protiv anemije.\r\nPodupiru oporavak kuje u postnatalnom razdoblju.\r\nPogodne za sve pse.\r\nNe sadrÅ¾e gluten, Å¾itarice niti mesno braÅ¡no.\r\nDodatak prehrani nije zamjena uravnoteÅ¾enoj prehrani.\r\nNarezane su na male komadiÄ‡e prikladne za upotrebu kao nagrade pri uÄenju.\r\nTvrde nagradice pas duÅ¾e Å¾vaÄe Å¡to ometa uÄenje, smanjuje koncentraciju.\r\nMekane nagradice pas pregrize, proguta i za kratko vrijeme moÅ¾e dobiti novu komandu i novu nagradu.\r\nNakon otvaranja pakiranja, dugo ostaju svjeÅ¾e i ukusne.\r\n\r\nSASTAV: 85% svjeÅ¾eg mesa i svjeÅ¾e ribe (svjeÅ¾e janjeÄ‡e meso 35%, svjeÅ¾e pileÄ‡e meso 20%, svjeÅ¾e svinjske jetre 20% i svjeÅ¾e ribe 10%),klice mahunarki 15% (zeleni graÅ¡ak Psilium sativum 3,76%, bijeli graÅ¡ak Psilium spp. 3,75% i leÄ‡a Lens Culinaris 1%), glicerol 3%, suhe alge 2,09%, suhi pivarski kvasac (Saccharomyces cerevisiae) 0,70%, glukozamin: 0,15%, kondroitin sulfat: 0,1%.\r\n\r\nANALITIÄŒKI SASTAV: bjelanÄevine 32%, sadrÅ¾aj masti 8%, sirovi pepeo 9,5%, vlakna 1%. Dodano/Kg proizvoda: cink 0,675g (cinkov amino acid kelat, hidrat 0,45%).\r\n\r\nUPUTA ZA UPOTREBU: dnevna koliÄina ne smije prijeÄ‡i 10% dnevnog obroka. Psu uvijek osigurajte pristup svjeÅ¾oj vodi. ÄŒuvanje: hladno, suho mjesto, zaÅ¡tiÄ‡eno od utjecaja sunÄeve svjetlosti.\r\n\r\nZemlja podrijetla: Å panjolska. ', 'psi', b'0'),
('5d26979a91f91', 'Ogrlica protiv buha i krpelja za maÄke i pse 1-8k', '204.95', 'BAYER Foresto ogrlica protiv buha i krpelja za maÄke i pse, 1-8 kg, 38 cm\r\n\r\nAntiparazitski ovratnik, imidakloprid, flumetrin\r\n\r\nIndikacije\r\nForestoÂ® se koristi za:\r\n-Suzbijanje i sprijeÄavanje infestacije maÄaka i pasa buhama tijekom 8 mjeseci.\r\n-PomoÄ‡ u suzbijanju i lijeÄenju alergijskog dermatitisa uzrokovanog ugrizima buha\r\n-Suzbijanje i sprijeÄavanje infestacije (repelentni uÄinak) krpeljima tijekom 8 mjeseci\r\n-Suzbijanje infestacije pasa pauÅ¡ima (Trichodectes canis)\r\n-Indirektna zaÅ¡tita od prijenosa uzroÄnika bolesti pasa koji se prenose vektorima (CVBD i FVBD).\r\n\r\nNaÄin primjene i doze\r\nJedan ovratnik priÄvrsti se Å¾ivotinji oko vrata.\r\n-MaÄkama i psima do 8 kg t.m. stavi se ForestoÂ® ovratnik duÅ¾ine 38 cm.\r\nOvratnik se neposredno prije primjene izvadi iz vreÄ‡ice. Å½ivotinji se ovratnik stavi oko vrata - u pravilu izmeÄ‘u\r\novratnika i vrata moraju se moÄ‡i ugurati 2 prsta. Kraj ovratnika provuÄe se kroz omÄu, a suviÅ¡ak se odreÅ¾e. Å½ivotinje moraju kontinuirano tijekom 8 mjeseci nositi ovratnik.\r\n\r\nKontraindikacije\r\nOvratnik se ne smije primjenjivati kod:\r\n-maÄiÄ‡a mlaÄ‘ih od 10 tjedana\r\n-Å¡tenadi mlaÄ‘e od 7 tjedana\r\n-jedinki preosjetljivih na djelatne ili pomoÄ‡ne tvari pripravka\r\n\r\nKarencija\r\nPripravak se ne upotrebljava za suzbijanje ektoparazita na Å¾ivotinjama koje se koriste za prehranu ljudi ili izradu namirnica.\r\n\r\nNaÄin izdavanja\r\nBez veterinarskog recepta.', 'psi', b'1'),
('5d2698df347c1', 'BRIT PREMIUM vreÄ‡ica, NjeÅ¾ni fileti u Å¾eleu Fam', '44.90', 'BRIT PREMIUM vreÄ‡ica, NjeÅ¾ni fileti u Å¾eleu Family Plate (12x85g)\r\n\r\nPotpuna hrana za odrasle maÄke, razni okusi (3x Piletina, 3x Govedina, 3x Losos, 3x Pastrva).\r\nBez umjetnih aroma. Bez konzervansa.\r\n\r\nS PILETINOM: SASTAV: meso i preraÄ‘evine Å¾ivotinjskog podrijetla 82% (piletina 8%), ekstrakt biljnih bjelanÄevina, preraÄ‘evine biljnog podrijetla (0,4% inulin), minerali, razni Å¡eÄ‡eri.\r\nS LOSOSOM: SASTAV: meso i preraÄ‘evine Å¾ivotinjskog podrijetla 74%, riba i preraÄ‘evine (fileti lososa 8%), ekstrakt biljnih bjelanÄevina, preraÄ‘evine biljnog podrijetla (0,4% inulin), minerali, razni Å¡eÄ‡eri.\r\nS GOVEDINOM: SASTAV: meso i preraÄ‘evine Å¾ivotinjskog podrijetla 82% (govedina 8%), ekstrakt biljnih bjelanÄevina, preraÄ‘evine biljnog podrijetla (0,4% inulin), minerali, razni Å¡eÄ‡eri.\r\nS PASTRVOM: SASTAV: meso i preraÄ‘evine Å¾ivotinjskog podrijetla 74%, riba i preraÄ‘evine (fileti pastrve 8%), ekstrakt biljnih bjelanÄevina, preraÄ‘evine biljnog podrijetla (0,4% inulin), minerali, razni Å¡eÄ‡eri.\r\n\r\nANALITIÄŒKI SASTAV: bjelanÄevine 8,5%, sadrÅ¾aj masti 4,5%, sirovi pepeo 2,0%, sirova vlaknina 0,3%, vlaga 82%.\r\nDODACI/kg: nutritivni dodaci: vitamin D3 (E671) 250IJ, vitamin E (alpha tokoferol, 3a700) 100mg, cink (3b606,cinkov sulfat, monohidrat) 10mg, mangan (E5, manganov oksid) 2mg, jod (3b201, kalijev jodid) 0,7mg, bakar (E4, bakrov sulfat, pentahidrat) 0,2mg, taurin (3a370) 450mg, biotin 0,1mg.\r\n\r\nUPUTA ZA HRANJENJE I ÄŒUVANJE:\r\n\r\nDnevna koliÄina hrane prikazana je u tablici hranjenja;\r\n(tjelesna masa maÄke/broj vreÄ‡ica na dan), podijeljena u dva dnevna obroka:\r\n\r\n2 - 5 kg / 0,5 - 1 vreÄ‡ica; koliÄinu posluÅ¾iti 2 puta dnevno.\r\n5 - 8 kg / 1 - 2 vreÄ‡ice; koliÄinu posluÅ¾iti 2 puta dnevno.\r\n\r\nSvjeÅ¾a i Äista voda neka je uvijek dostupna.\r\nPosluÅ¾iti na sobnoj temperaturi.\r\nNakon otvaranja Äuvajte u hladnjaku.\r\n\r\nZEMLJA PORIJEKLA: EU (R. ÄŒeÅ¡ka) ', 'maÄke', b'0'),
('5d2699171d137', 'Hrskava poslastica za mace od piletine, 100 g', '9.95', 'BRIT CARE Superfruits Chicken\r\n\r\nDopunska hrana za odrasle maÄke.\r\nGrain Free - bez Å¾itarica, Superhrana.\r\nHrskava poslastica za maÄke, od piletine, s pasjim trnom i borovnicom.\r\n\r\nSASTAV: braÅ¡no piletine (40%), Å¾uti graÅ¡ak, pileÄ‡a mast (konzervirana tokoferolima, 8%), slatki krumpir, hidolizirane bjelanÄevine piletine (7%), Å¡korb tapioke, pasji trn (4%), borovnica (4%), hidrolizirana pileÄ‡a jetra, lososovo ulje (2%), laneno sjeme (1%).\r\nANALITIÄŒKI SASTAV: bjelanÄevine 35%, sadrÅ¾aj masti 15%, vlaga 10%, sirovi pepeo 6,6%, sirova vlaknina 1,5%, kalcij 1%, fosfor 0,8%, natrij 0,3%.\r\nMetaboliÄka energija 3850 kcal/kg.\r\n\r\nUPUTE ZA HRANJENJE I ÄŒUVANJE: Ponudite poslasticu iz ruke kao nagradu i pruÅ¾ite maci ugodu druÅ¾enja.\r\nSlijedite preporuÄene dnevne koliÄine navedene u tablici hranjenja, (tjelesna masa maÄke u kg/broj komada na dan):\r\n\r\n1 - 2kg / 8;\r\n3 - 4kg / 12;\r\n4 - 6kg / 15;\r\n6kg+ / 18.\r\n\r\nSvjeÅ¾a voda neka je uvijek dostupna vaÅ¡em ljubimcu.\r\nÄŒuvati na suhom i hladnom mjestu. ', 'maÄke', b'1'),
('5d26994a5cdd5', 'FLAMINGO Grebalica za maÄke Amethyst beÅ¾ 30x30x5', '99.00', '', 'maÄke', b'0'),
('5d26997fcf73e', 'Pijesak za maÄke, upijajuÄ‡i 10l', '79.95', 'CATSAN Pijesak za maÄke Catlitter Hygiene 10l, higijenska stelja.\r\n\r\nUPUTE ZA UPORABU:\r\nNapunite maÄji toalet sa CATSANÂ® granulatom do dubine od 5 cm.\r\nÄŒvrsti otpad uklonite individualno.\r\nMijenjajte CATSANÂ® redovito.\r\nUkoliko imate podno grijanje, toalet za maÄke treba biti postavljen na podlogu od izolacijskog materijala (drvo, stiropor).\r\n\r\nDrÅ¾ava podrijetla/ProizvoÄ‘aÄ: Mars GmbH, 27281 Verden/Aller, NjemaÄka ', 'maÄke', b'0'),
('5d2699c453694', 'IgraÄka za mace MiÅ¡ s perjem, zvuÄna, razne boj', '3.95', '', 'maÄke', b'0'),
('5d2699ea63cac', 'ALL FOR PAWS Lambswool Krevet za mace Snuggle Bed ', '139.00', '', 'maÄke', b'0'),
('5d269a1c152a1', 'Melaminska zdjelica za mace Love Bowl', '33.90', '', 'maÄke', b'0'),
('5d269b2ecc56a', 'DOLLY Hrana za kanarince 450g', '8.95', 'Potpuna hrana za kanarince s vitaminima i mineralima.\r\nKanarinac je jedna od naÅ¡ih omiljenih ptica. Njegov pijev se Äuje daleko, te psiholoÅ¡ki maknuti ljude od zatvorenosti. TakoÄ‘er moÅ¾e doÄarat prirodu u naÅ¡em domu.\r\nDolly hrana za kanarince je savrÅ¡en izbor s obzirom na fizioloÅ¡ke potrebe ptice.\r\n\r\nSASTAV: crveni proso, Å¾uti proso, svijetlo sjeme, uljana repica, muhar u prikladnoj mjeÅ¡avini.\r\n\r\nPotrebno je hraniti jedanput dnevno, te osigurat ptici svjeÅ¾u vodu.\r\nHranjenje se moÅ¾e zavrÅ¡it sa povrÄ‡em poput salate, te naribanom mrkvom ili jabukom.\r\nDrÅ¾ati na hladnom i suhom mjestu. ', 'ptice', b'1'),
('5d269b590809a', 'DIAFARM Vitaminski sirup za ptice, 15ml', '39.25', 'DIAFARM Vitaminski sirup za ptice\r\n\r\nSadrÅ¾i hranjive tvari vaÅ¾ne u razdoblju mitarenja.\r\n\r\nDaje lijepo perje i pridonosi dobrom opÄ‡em stanju ptica.\r\n\r\nUPUTA ZA UPORABU: Prije uporabe protresti. Dodati u svjeÅ¾u vodu za piÄ‡e ili umijeÅ¡ati u hranu.\r\nPapigice, kanarinci i druge male ptice 5-6 kapi na dan. Nimfe i velike papige 15-30 kapi na dan.\r\n\r\nÄŒuvati na sobnoj temperaturi i van dohvata djece.\r\n\r\nSASTAV: Vitamini i minerali. ', 'ptice', b'0'),
('5d269b8cf35c2', 'DUVO+ Krletka Ara, za veÄ‡e papige, 54x34x65,5cm, ', '419.00', 'Visoko kvalitetna krletka za velike papige.\r\n\r\nHranilica i pojilica ukljuÄene.\r\n\r\n2 preÄke za stajanje.\r\n\r\nLadica u podnoÅ¾ju - jednostavno ÄiÅ¡Ä‡enje.\r\n\r\nPokrov se otvara pretvarajuÄ‡i tako krletku u otovrenu stajalicu za papigu. ', 'ptice', b'0'),
('5d269bbfc69d6', 'CHIPSI Extra Medium Stelja za ptice i gmazove, drv', '46.95', 'CHIPSI Extra Medium\r\n\r\nOpis; Chipsi Extra je srednje zrnata drvena stelja za kuÄ‡ne ljubimce, za manje reptile, velike ptice i kornjaÄe.\r\n\r\nZa terarije i voliere.\r\n\r\nIzraÄ‘ena od netretiranog bukovog drveta s jako upijajuÄ‡im strugotinama koje osiguravaju meko i Äisto staniÅ¡te.\r\n\r\nNe stvara praÅ¡inu Å¡to je vrlo bitno za Å¾ivotinje s osjetljivim organima za disanje.\r\n\r\nOdlikuje se velikim kapacitetom upijanja vlage i neugodnih mirisa.\r\n\r\nStelja je apsolutno suha i sterilna. Bez kemijskih dodataka.\r\n\r\nÄŒuvajte zatvoreno, na suhom i hladnom mjestu.\r\n\r\nZemlja porijekla: NjemaÄka. ', 'ptice', b'0'),
('5d269bf19fb4a', 'FLAMINGO Penjalica drvena za ptice razne dimenzije', '29.50', '', 'ptice', b'0'),
('5d269c9cedb4c', 'Akvarij stakleni 100x30x40cm / 6mm', '399.00', '', 'ribice', b'1'),
('5d269ce0d3025', 'DAJANA Basic Tropica hrana za tropske ribice 100ml', '28.80', 'BIOFILTER tabs - visoko kvalitetna kultura bakterija s enzimima u formi tableta\r\n\r\nSvaka tableta sadrÅ¾i milione kolonija bakterija zaduÅ¾enih za bioloÅ¡ko ÄiÅ¡Ä‡enje akvarija. Dodajte tabletu u novi akvarij ili ju umetnite izmeÄ‘u filtracijskih elemenata u filteru. NajuÄinkovitija bioloÅ¡ka filtracija postiÅ¾e se u kombinaciji s prikladnim filtracijskim uloÅ¡kom.\r\nUputa za upotrebu:\r\nPoÄetna doza; mali filteri do 150 cm3 - 2 tablete, srednji filteri iznad 150 cm3 - 4 tablete, veliki filteri iznad 300 cm3 - 6 tableta.\r\nDoza za odrÅ¾avanje je 1 - 3 tablete za svaki filter svaka 2 tjedna. Dodati 1 - 3 tablete nakon svake intervencije u akvariju.\r\nDoza odrÅ¾avanja za jezerca iznosi 10 tableta/m3/ mjeseÄno.\r\nUpozorenje: Ne upotrebljavati kod Å¾ivotinja namijenjenih za ishranu ljudi! ÄŒuvajte izvan dosega djece. ', 'ribice', b'0'),
('5d269d141884c', 'DAJANA Biofilter tablete bioloÅ¡ki dodatak za akva', '28.80', 'BIOFILTER tabs - visoko kvalitetna kultura bakterija s enzimima u formi tableta\r\n\r\nSvaka tableta sadrÅ¾i milione kolonija bakterija zaduÅ¾enih za bioloÅ¡ko ÄiÅ¡Ä‡enje akvarija. Dodajte tabletu u novi akvarij ili ju umetnite izmeÄ‘u filtracijskih elemenata u filteru. NajuÄinkovitija bioloÅ¡ka filtracija postiÅ¾e se u kombinaciji s prikladnim filtracijskim uloÅ¡kom.\r\nUputa za upotrebu:\r\nPoÄetna doza; mali filteri do 150 cm3 - 2 tablete, srednji filteri iznad 150 cm3 - 4 tablete, veliki filteri iznad 300 cm3 - 6 tableta.\r\nDoza za odrÅ¾avanje je 1 - 3 tablete za svaki filter svaka 2 tjedna. Dodati 1 - 3 tablete nakon svake intervencije u akvariju.\r\nDoza odrÅ¾avanja za jezerca iznosi 10 tableta/m3/ mjeseÄno.\r\nUpozorenje: Ne upotrebljavati kod Å¾ivotinja namijenjenih za ishranu ljudi! ÄŒuvajte izvan dosega djece. ', 'ribice', b'0'),
('5d269d524f586', 'Unutarnji filter Fan Plus', '109.00', 'AQUAEL FAN unutarnji filter je dizajniran za ÄiÅ¡Ä‡enje i prozraÄivanje vode akvarija. Jednostavan za instalaciju i rad te visoku uÄinkovitost na relativno niskoj razini potroÅ¡nje energije. Idealan Äak i za najmanje akvarije (Mikro model).\r\nUnutarnji filter se moÅ¾e prilagoditi bez umetanja ruku u vodu, buduÄ‡i da se kupola s ergonomskim kontrolnim kotaÄiÄ‡em moÅ¾e postaviti iznad povrÅ¡ine vode.\r\nSmjer za istjecanje vode se moÅ¾e podesiti do 75 Â°. ', 'ribice', b'0'),
('5d269d8b802a2', 'AQUAEL Comfort zone FIX grijaÄ', '119.00', 'Aquael Comfort zone FIX akvarijski grijaÄ\r\n\r\nAquaEl Comfort Zone Fix grijaÄ namjenjen je zagrijavanju vode u akvarijima.\r\nSvaki grijaÄ nakon proizvodnje kompjuterski je testiran u profesionalnom Aquael laboratoriju a potom se puÅ¡ta u daljnju prodaju.\r\nGrijaÄ odrÅ¾ava temperaturu vode u akvariju na 25Â° C, s toÄnoÅ¡Ä‡u od +/-0,5Â°C. Comfort Zone Fix grijaÄi mogu se koristiti u morskim i slatkovodnom akvarijima.\r\n\r\nVolumen akvarija - Snaga grijaÄa\r\n\r\n10-25L - 25W\r\n26-50L - 50W\r\n51-75L - 75W\r\n76-100L - 100W ', 'ribice', b'0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administratori`
--
ALTER TABLE `administratori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `korisnik`
--
ALTER TABLE `korisnik`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kosarica_artikli`
--
ALTER TABLE `kosarica_artikli`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proizvodi`
--
ALTER TABLE `proizvodi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administratori`
--
ALTER TABLE `administratori`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
