<?php
if(!isset($_COOKIE["AdminLoggedIn"])) {
    header("Location: admin.php");
    exit();
} 
?>

<html>

<head>
    <link rel="stylesheet" type="text/css"  href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="script.js"></script>
</head>



<body>
<div class="content loginregister">
    <h2 style="text-align:center">Administrator - dodaj novi proizvod</h2>

    <form enctype="multipart/form-data" action="" method="post">
            <br>
            Ime proizvoda:<br>
            <input type="text" name="name"><br><br>
            Cijena:<br>
            <input type="number" step="0.01" name="price"><br><br>
			Opis:<br>
            <textarea rows="5" cols="25" name="description" ></textarea><br><br>
            Kategorija:<br>
            <select name="category">
                <option value="psi">Psi</option>
                <option value="mačke">Mačke</option>
                <option value="ptice">Ptice</option>
                <option value="ribice">Ribice</option>
            </select> <br><br>
            Slika:<br>
            <input type="file" name="picture"><br><br>
            <input type="submit" name="act" value="Dodaj">
        </form>
</div>

<?php
	include('connect.php');
	if (isset($_POST['act'])) {
	    $name = $_POST['name'];
	    $price = $_POST['price'];
	    $description = $_POST['description'];
        $category = $_POST['category'];

        if($name === ""){
            echo "Nije uneseno ime.";
            die();
        }
        if($price === ""){
            echo "Nije unesena cijena.";
            die();
        }
    
        $id = uniqid();
	    if(!move_uploaded_file($_FILES["picture"]["tmp_name"],getcwd() . "\SlikeProizvoda\\" . $id)){
            echo "Slika nije učitana.";
            die();
        }
        else{
	        $sql = "INSERT INTO proizvodi VALUES ('$id','$name','$price','$description', '$category', 0);";
	        if($conn->query($sql) === FALSE) {
                $conn->close();
                unlink(getcwd() . "\SlikeProizvoda\\" . $id);
		        echo "Došlo je do pogreške";		
	        }
	        else{
                $conn->close();
                header("Location: admincontrolpanel.php");
		        die();		
            }
        }
	}
	
 ?>
</body>

</html>