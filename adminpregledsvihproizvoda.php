<?php
if(!isset($_COOKIE["AdminLoggedIn"])) {
    header("Location: admin.php");
    exit();
} 
?>

<html>

<head>
    <link rel="stylesheet" type="text/css"  href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="script.js"></script>
    <script src="deleteitem.js"></script>
</head>



<body>
<div class="content">
    <h2 style="text-align:center">Administrator - pregled svih proizvoda</h2>
    <div class="items">
    <?php
    include('connect.php');
	$sql = "SELECT * FROM proizvodi";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
            echo "<div class='item' id='item". $row["id"] . "'>id: " . $row["id"] . "<br>" . "Ime: " . $row["ime"] . "<br>" . "Cijena: " . $row["cijena"] 
            . "kn<br>" . "Kategorija: " . $row["kategorija"] . "<br>" . "Opis: " . $row["opis"] . "<br>"
            . "<img src='image.php?path=". getcwd() . "\SlikeProizvoda\\" . $row["id"] . "'/>" . "<br><br>" 
            . "<button id='" . $row["id"] . "' onclick='DeleteItem(this.id)'>Obriši</button> "
            . "<button id='edit" . $row["id"] . "' onclick='EditItem(this.id)'>Uredi</button> ";
            if($row["naslovna"] == 0){
                echo "<button id='settohomepage" . $row["id"] . "' onclick='SetItemToHomepage(this.id)'>Postavi na naslovnu</button>";
            }
            else{
                echo "<button id='settohomepage" . $row["id"] . "' onclick='SetItemToHomepage(this.id)'>Ukloni s naslovne</button>";
            }
            echo "</div>";
		}
	} else {
		echo "Nema proizvoda";
		}
	$conn->close();
    ?>
    </div>
</div>
</body>

</html>