function Logout(){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          if(this.responseText == "success"){
            window.location.replace("admin.php");
          }
      }
    };
    xmlhttp.open("GET", "adminsignout.php", true);
    xmlhttp.send();
}