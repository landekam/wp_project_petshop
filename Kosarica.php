<html>

<head>
    <link rel="stylesheet" type="text/css"  href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="script.js"></script> 
    <script src="gotoitem.js"></script>
    <script src="cartactions.js"></script>
</head>

<body>
<div class="topnav" id="Topnav">
    <a href="index.php"><i class="fa fa-home fa-lg"></i></a>
    <a href="Psi.php">Psi</a>
    <a href="Macke.php">Mačke</a>
    <a href="Ptice.php">Ptice</a>
    <a href="Ribice.php">Ribice</a>
    <a href="Kontakt.php">Kontakt</a>
    <a href="Kosarica.php" class="active"><i class="fa fa-shopping-cart fa-lg"></i></a>
    <a href="Login.php"><i class="fa fa-user fa-lg"></i>
    <a href="javascript:void(0);" class="icon" onclick="DropdownMenu()">
    <i class="fa fa-bars fa-lg"></i>
  </a>
</div>

<div class="content">
<h1 style="color:#93827f; text-align:center;">Košarica</h1>
<?php
if(isset($_COOKIE["LoggedIn"])) {
  setcookie("LoggedIn", $_COOKIE["LoggedIn"], time()+3600);
} 
else{
  header("Location: Login.php");
  exit();
}

include('connect.php');
$salt = $_COOKIE["LoggedIn"];
$sql = "SELECT id FROM korisnik WHERE salt='$salt'";
$result = $conn->query($sql);
while($row = $result->fetch_assoc()) {
    $user_id = $row["id"];
}

$totalcost = 0;

$sql = "SELECT * FROM kosarica_artikli WHERE korisnik_id='$user_id'";
$result = $conn->query($sql);
while($row = $result->fetch_assoc()) {
    $product_id = $row["proizvod_id"];
    $sql = "SELECT * FROM proizvodi WHERE id='$product_id'";
    $productresult = $conn->query($sql);
    while($productrow = $productresult->fetch_assoc()) {
      echo "<div class='cartitem' id='item" . $row["id"] . "'>";
      echo "<img id='". $productrow["id"] ."' onclick='GoToItem(this.id)' style='max-width:150px;max-height:150px;cursor:pointer;display:inline-block;' src='image.php?path=". getcwd() . "\SlikeProizvoda\\" . $productrow["id"] . "'/>";
      echo " <p id='". $productrow["id"] ."' style='color:#6ba292;width:500px;cursor:pointer;display:inline-block;' onclick='GoToItem(this.id)'>" . $productrow["ime"] . "</p>";
      echo " <p id='price" . $row["id"] . "' style='color:#93827f;width:150px;display:inline-block;'>" . $productrow["cijena"] . " kn</p>";
      echo "<div class='adjustamount'>
      <button id='" . $row["id"] ."' onclick='ReduceAmount(this.id)'>-</button>
      <p id='amount" . $row["id"] . "'>" . $row["kolicina"] . "</p>
      <button id='" . $row["id"] ."' onclick='IncreaseAmount(this.id)'>+</button></div>";
      echo "<button class='remove' id='" . $row["id"] ."' onclick='RemoveItem(this.id)'>Ukloni iz košarice</button>";
      echo "<hr></div>";
      $totalcost += $productrow["cijena"] * $row["kolicina"];
    }
}
echo "<p id='totalprice' style='font-size:25px;float:right;color:#93827f;'>Ukupno: " . $totalcost . "kn</p>"
?>
</div>


</body>

</html>