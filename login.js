function Login(){
    var email = document.getElementById("Email").value;
    var password = document.getElementById("Password").value;

    if(email === ""){
        alert("Molim, unesite e-mail u odgovarajuće polje.");
        return;
    }
    if(password === ""){
        alert("Molim, unesite zaporku u odgovarajuće polje.");
        return;
    }

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          if(this.responseText == "success"){
            window.location.replace("index.php");
          }
          else{
            document.getElementById("result").innerHTML = this.responseText;
          }
      }
    };
    xmlhttp.open("GET", "signin.php?email=" + email + "&password=" + password, true);
    xmlhttp.send();
}