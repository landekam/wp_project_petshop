<html>

<head>
    <link rel="stylesheet" type="text/css"  href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="script.js"></script>
    <script src="adminlogin.js"></script>
</head>



<body>

<div class="content loginregister">
    <h2>Administrator - prijava</h2>
    <br>
    <p>Korisničko ime:</p>
    <input type="name" name="Username" id="Username" value=""><br>
    <p>Zaporka:</p>
    <input type="password" name="Password" id="Password" value=""><br>
    <br>
    <button onclick="Login()">Prijava</button>
    <br>
    <br>
    <p id="result"></p>
</div>

<?php
if(isset($_COOKIE["AdminLoggedIn"])) {
    header("Location: admincontrolpanel.php");
    exit();
} 
?>

</body>

</html>