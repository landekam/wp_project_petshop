<html>

<head>
    <link rel="stylesheet" type="text/css"  href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="script.js"></script>
</head>



<body>
<div class="content loginregister">
    <h2 style="text-align:center">Administrator - dodaj novi proizvod</h2>

    <?php
        if(!isset($_COOKIE["AdminLoggedIn"])) {
            header("Location: admin.php");
            exit();
        } 

        $id =  $_GET['id'];
        include('connect.php');
	    $sql = "SELECT * FROM proizvodi WHERE id='$id'";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                echo "<form enctype='multipart/form-data' action='' method='post'>
                <br>
                Ime proizvoda:<br>
                <input type='text' name='name' value='" . $row['ime'] . "'><br><br>
                Cijena:<br>
                <input type='number' step='0.01' name='price' value='" . $row['cijena'] . "'><br><br>
                Opis:<br>
                <textarea rows='5' cols='25' name='description'>" . $row['opis'] . "</textarea><br><br>
                Kategorija:<br>
                <select name='category'>
                    <option value='psi'>Psi</option>
                    <option value='mačke'>Mačke</option>
                    <option value='ptice'>Ptice</option>
                    <option value='ribice'>Ribice</option>
                </select> <br><br>
                Slika:<br>
                <img style='max-width:250px;max-height:250px;' src='image.php?path=". getcwd() . "\SlikeProizvoda\\" . $row["id"] . "'/>" . "<br>
                <input type='file' name='picture'><br><br>
                <input type='submit' name='act' value='Izmjeni'>
            </form>";
            }
        }
    ?>    
    </div>

    <?php
	include('connect.php');
	if (isset($_POST['act'])) {
	    $name = $_POST['name'];
	    $price = $_POST['price'];
	    $description = $_POST['description'];
        $category = $_POST['category'];

        if($name === ""){
            echo "Nije uneseno ime.";
            die();
        }
        if($price === ""){
            echo "Nije unesena cijena.";
            die();
        }
        
        move_uploaded_file($_FILES["picture"]["tmp_name"],getcwd() . "\SlikeProizvoda\\" . $id);
        
	    $sql = "UPDATE proizvodi SET ime='$name', cijena='$price', opis='$description', kategorija='$category' WHERE id='$id';";
	    if($conn->query($sql) === FALSE) {
            $conn->close();
		    echo "Došlo je do pogreške";		
	    }
	    else{
            $conn->close();
            header("Location: adminpregledsvihproizvoda.php");
		    die();		
        }
        
	}
	
 ?>

    </body>

</html>