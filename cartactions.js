function ReduceAmount(id){
    var amount = document.getElementById("amount" + id).innerHTML;
    if(amount == 1){
        alert("Količina ne može biti manja od 1.");
        return;
    }
    amount--;

    var currentTotalPrice = document.getElementById("totalprice").innerHTML;
    var itemPrice = document.getElementById("price" + id).innerHTML;
    
    currentTotalPrice = currentTotalPrice.substring(8,currentTotalPrice.indexOf("kn"));
    currentTotalPrice = parseFloat(currentTotalPrice);
    itemPrice = parseFloat(itemPrice);
    var newPrice = currentTotalPrice - itemPrice;
    
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          if(this.responseText == "success"){
            document.getElementById("amount" + id).innerHTML = amount;
            document.getElementById("totalprice").innerHTML = "Ukupno: " + newPrice.toFixed(2) + "kn";
          }
      }
    };
    xmlhttp.open("GET", "adjustamount.php?id=" + id + "&amount=" + amount, true);
    xmlhttp.send();
}

function IncreaseAmount(id){
    var amount = document.getElementById("amount" + id).innerHTML;
    amount++;

    var currentTotalPrice = document.getElementById("totalprice").innerHTML;
    var itemPrice = document.getElementById("price" + id).innerHTML;
    
    currentTotalPrice = currentTotalPrice.substring(8,currentTotalPrice.indexOf("kn"));
    currentTotalPrice = parseFloat(currentTotalPrice);
    itemPrice = parseFloat(itemPrice);
    var newPrice = currentTotalPrice + itemPrice;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          if(this.responseText == "success"){
            document.getElementById("amount" + id).innerHTML = amount;
            document.getElementById("totalprice").innerHTML = "Ukupno: " + newPrice.toFixed(2) + "kn";
          }
      }
    };
    xmlhttp.open("GET", "adjustamount.php?id=" + id + "&amount=" + amount, true);
    xmlhttp.send();
}

function RemoveItem(id){
    var xmlhttp = new XMLHttpRequest();

    var currentTotalPrice = document.getElementById("totalprice").innerHTML;
    var itemPrice = document.getElementById("price" + id).innerHTML;
    
    currentTotalPrice = currentTotalPrice.substring(8,currentTotalPrice.indexOf("kn"));
    currentTotalPrice = parseFloat(currentTotalPrice);
    itemPrice = parseFloat(itemPrice);
    var newPrice = currentTotalPrice - itemPrice * document.getElementById("amount" + id).innerHTML;

    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          if(this.responseText == "success"){
            document.getElementById("item" + id).remove();
            document.getElementById("totalprice").innerHTML = "Ukupno: " + newPrice.toFixed(2) + "kn";
          }
      }
    };
    xmlhttp.open("GET", "removefromcart.php?id=" + id, true);
    xmlhttp.send();
}