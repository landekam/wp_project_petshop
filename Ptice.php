<html>

<head>
    <link rel="stylesheet" type="text/css"  href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="script.js"></script>
    <script src="gotoitem.js"></script>
</head>

<body>
<div class="topnav" id="Topnav">
    <a href="index.php"><i class="fa fa-home fa-lg"></i></a>
    <a href="Psi.php">Psi</a>
    <a href="Macke.php">Mačke</a>
    <a href="Ptice.php" class="active">Ptice</a>
    <a href="Ribice.php">Ribice</a>
    <a href="Kontakt.php">Kontakt</a>
    <a href="Kosarica.php"><i class="fa fa-shopping-cart fa-lg"></i></a>
    <a href="Login.php"><i class="fa fa-user fa-lg"></i>
    <a href="javascript:void(0);" class="icon" onclick="DropdownMenu()">
    <i class="fa fa-bars fa-lg"></i>
  </a>
</div>

<div class="content">
<h1 style="color:#93827f; text-align:center;">Ptice</h1>
<div class="items">

<?php
if(isset($_COOKIE["LoggedIn"])) {
  setcookie("LoggedIn", $_COOKIE["LoggedIn"], time()+3600);
}

include('connect.php');
	$sql = "SELECT * FROM proizvodi WHERE kategorija='ptice'";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
            echo "<div class='shopitem'>"  . "<img id='" . $row["id"] . "' onclick='GoToItem(this.id)' src='image.php?path=". getcwd() . "\SlikeProizvoda\\" . $row["id"] . "'/>"
            . "<p id='" . $row["id"] . "' onclick='GoToItem(this.id)' class='name' >" . $row["ime"] . "</p><p class='price'>" . $row["cijena"] . "kn</p>";            
            echo "</div>";
		}
	}
	$conn->close();
?>
</div>
</div>
</body>

</html>