function Login(){
    var username = document.getElementById("Username").value;
    var password = document.getElementById("Password").value;

    if(username === ""){
        alert("Unesite korisničko u odgovarajuće polje.");
        return;
    }
    if(password === ""){
        alert("Unesite zaporku u odgovarajuće polje.");
        return;
    }

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          if(this.responseText == "success"){
            window.location.replace("admincontrolpanel.php");
          }
          else{
            document.getElementById("result").innerHTML = this.responseText;
          }
      }
    };
    xmlhttp.open("GET", "adminsignin.php?username=" + username + "&password=" + password, true);
    xmlhttp.send();
}