<?php
if(!isset($_COOKIE["AdminLoggedIn"])) {
    header("Location: admin.php");
    exit();
} 
?>

<html>

<head>
    <link rel="stylesheet" type="text/css"  href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="script.js"></script>
    <script src="adminlogout.js"></script>
</head>



<body>

<div class="content loginregister">
    <h2>Administrator - kontrolna ploča</h2>
    <br>
    <button onclick="window.location.href='admindodajproizvod.php'">Dodaj novi proizvod</button>
    <br>
    <br>
    <button onclick="window.location.href='adminpregledsvihproizvoda.php'">Pregled svih proizvoda</button>
    <br>
    <br>
    <button onclick="Logout()">Odjavi se</button>
</div>

</body>

</html>