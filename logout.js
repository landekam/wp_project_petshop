function Logout(){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          if(this.responseText == "success"){
            window.location.replace("index.php");
          }
      }
    };
    xmlhttp.open("GET", "signout.php", true);
    xmlhttp.send();
}

function SaveChanges(){
    var phoneNumber = document.getElementById("PhoneNumber").value;
    var city = document.getElementById("City").value;
    var address = document.getElementById("Address").value;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("result").innerHTML = this.responseText;
      }
    };
    xmlhttp.open("GET", "changeuserinfo.php?phoneNumber=" + phoneNumber + "&city=" + city + "&address=" + address, true);
    xmlhttp.send();
}