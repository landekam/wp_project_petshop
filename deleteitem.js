function DeleteItem(id){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          if(this.responseText == "success"){
            document.getElementById("item" + id).remove();
          }
          else{
            document.getElementById(id).innerHTML = "Nije obrisano";
          }
      }
    };
    xmlhttp.open("GET", "deleteitem.php?id=" + id, true);
    xmlhttp.send();
}

function EditItem(id){
    id = id.substring(4);
    window.location.href = "adminuredi.php?id=" + id;
}

function SetItemToHomepage(id){
    itemId = id.substring(13);
    if(document.getElementById(id).innerHTML == "Postavi na naslovnu"){
        naslovna = 1;
        
    }
    else{
        naslovna = 0;
    }
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {  
            if(this.responseText == "success"){
                if(document.getElementById(id).innerHTML == "Postavi na naslovnu"){
                    document.getElementById(id).innerHTML = "Ukloni s naslovne";
                }
                else{
                    document.getElementById(id).innerHTML = "Postavi na naslovnu";
                }
            }    
        }
      };
    xmlhttp.open("GET", "setremovefromhomepage.php?id=" + itemId + "&naslovna=" + naslovna, true);
    xmlhttp.send();
    
}