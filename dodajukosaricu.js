function DodajUKosaricu(id){
    var amount = document.getElementById("kolicina").value;
    if(isNaN(amount) || amount < 1){
        alert("Količina treba biti pozitivan broj");
        return;
    }

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          if(this.responseText == "success"){
            window.location.href = "Kosarica.php";
          }
          else if(this.responseText == "not logged in"){
            window.location.href = "Login.php";
          }
          else{
              alert(this.responseText)
          }
      }
    };
    xmlhttp.open("GET", "addtocart.php?productid=" + id + "&amount=" + amount , true);
    xmlhttp.send();
}