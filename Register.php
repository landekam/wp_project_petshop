<html>

<head>
    <link rel="stylesheet" type="text/css"  href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="script.js"></script>
    <script src="register.js"></script>

</head>

<body>
<div class="topnav" id="Topnav">
    <a href="index.php"><i class="fa fa-home fa-lg" ></i></a>
    <a href="Psi.php">Psi</a>
    <a href="Macke.php">Mačke</a>
    <a href="Ptice.php">Ptice</a>
    <a href="Ribice.php">Ribice</a>
    <a href="Kontakt.php">Kontakt</a>
    <a href="Kosarica.php"><i class="fa fa-shopping-cart fa-lg" ></i></a>
    <a href="Login.php" class="active"><i class="fa fa-user fa-lg" ></i>
    <a href="javascript:void(0);" class="icon" onclick="DropdownMenu()">
    <i class="fa fa-bars fa-lg" ></i>
  </a>
</div>

<div class="content loginregister">
<br>

    <p>E-mail:</p>
    <input type="email" name="Email" value="" id="Email"><br>
    <p>Zaporka:</p>
    <input type="password" name="Password" value="" id="Password"><br>
    <p>Ponovite zaporku:</p>
    <input type="password" name="PasswordRepeated" value="" id="PasswordRepeated"><br>
    <p>Ime:</p>
    <input type="name" name="FirstName" value="" id="FirstName"><br>
    <p>Prezime:</p>
    <input type="name" name="LastName" value="" id="LastName"><br>
    <p>Broj mobitela:</p>
    <input type="tel" name="PhoneNumber" id="PhoneNumber" value=""><br>
    <p>Grad:</p>
    <input type="name" name="City" id="City" value="""><br>
    <p>Adresa:</p>
    <input type="name" name="Address" id="Address" value=""><br>
    <br>
    <button onclick="CheckRegisterInfo()">Registriraj se</button>
    <br>
<p id="result"></p>
</div>

</body>

</html>

