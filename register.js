function CheckRegisterInfo(){
    var email = document.getElementById("Email").value;
    if(CheckEmail(email) === null){
      return;
    }
  
    var firstName = document.getElementById("FirstName").value;
    if(firstName === ""){
      alert("Molim, unesite ime u odgovarajuće polje.");
      return;
    }
  
    var lastName = document.getElementById("LastName").value;
    if(lastName === ""){
      alert("Molim, unesite prezime u odgovarajuće polje.");
      return;
    }
  
    var password = document.getElementById("Password").value;
    if(CheckPassword(password) === null){
      return;
    }
    var passwordRepeated = document.getElementById("PasswordRepeated").value;
    if(password !== passwordRepeated){
      alert("Zaporke nisu jednake.");
      return;
    }

    var phoneNumber = document.getElementById("PhoneNumber").value;
    var city = document.getElementById("City").value;
    var address = document.getElementById("Address").value;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("result").innerHTML = this.responseText;
      }
    };
    xmlhttp.open("GET", "registration.php?email=" + email + "&password=" + password + "&firstName=" + firstName
    + "&lastName=" + lastName + "&phoneNumber=" + phoneNumber + "&city=" + city + "&address=" + address, true);
    xmlhttp.send();
  }
  
  function CheckEmail(email){
    if(email === ""){
      alert("Molim, unesite e-mail u odgovarajuće polje.");
      return null;
    }
    var indexOfEt = email.indexOf("@");
    if(indexOfEt === -1){
      alert("E-mail ne sadrži '@' .");
      return null;
    }
    if(indexOfEt < 2){
      alert("E-mail prekratak prije '@' .");
      return null;
    }
    var emailAfterEt = email.substring(indexOfEt+1);
    indexOfDot = emailAfterEt.indexOf(".");
    if(indexOfDot === -1){
      alert("E-mail ne sadrži '.' nakon '@' .");
      return null;
    }
    if(indexOfDot < 2){
      alert("E-mail prekratak između '@' i '.' .");
      return null;
    }
    var emailAfterDot = emailAfterEt.substring(indexOfDot+1);
    if(emailAfterDot.length < 2){
      alert("E-mail prekratak nakon '.' .");
      return null;
    }
  }
  
  function CheckPassword(password){
    if(password === ""){
      alert("Molim, unesite zaporku u odgovarajuće polje.");
      return null;
    }
    if(password.length < 6){
      alert("Zaporka je prekratka, treba imati barem 6 znakova.");
      return null;
    }
    if(password.toLowerCase() === password){
      alert("Zaporka treba sadržavati barem jedno veliko slovo.");
      return null;
    }
    if(password.toUpperCase() === password){
      alert("Zaporka treba sadržavati barem jedno malo slovo.");
      return null;
    }
    if(password.indexOf("0") === -1 && password.indexOf("1") === -1 && password.indexOf("2") === -1
    && password.indexOf("3") === -1 && password.indexOf("4") === -1 && password.indexOf("5") === -1
    && password.indexOf("6") === -1 && password.indexOf("7") === -1 && password.indexOf("8") === -1
    && password.indexOf("9") === -1){
      alert("Zaporka treba sadržavati barem jednu znamenku.");
      return null;
    }
    if(password.indexOf("!") === -1 && password.indexOf("#") === -1 && password.indexOf("$") === -1
    && password.indexOf("%") === -1 && password.indexOf("&") === -1 && password.indexOf("(") === -1
    && password.indexOf(")") === -1 && password.indexOf("*") === -1 && password.indexOf("+") === -1
    && password.indexOf("-") === -1 && password.indexOf(".") === -1 && password.indexOf(",") === -1
    && password.indexOf(":") === -1 && password.indexOf(";") === -1 && password.indexOf("/") === -1
    && password.indexOf(">") === -1 && password.indexOf("<") === -1 && password.indexOf("=") === -1
    && password.indexOf("?") === -1 && password.indexOf("@") === -1 && password.indexOf("\\") === -1){
      alert("Zaporka treba sadržavati barem jedan poseban znak(!, #, $, %, &, (, ), *, +, -, ., , , :, ;, /, >, <, =, ?, @, \\).");
      return null;
    }
    
  }