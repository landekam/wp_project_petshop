<html>

<head>
    <link rel="stylesheet" type="text/css"  href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="script.js"></script>
    <script src="login.js"></script>
</head>



<body>

<div class="topnav" id="Topnav">
    <a href="index.php"><i class="fa fa-home fa-lg"></i></a>
    <a href="Psi.php">Psi</a>
    <a href="Macke.php">Mačke</a>
    <a href="Ptice.php">Ptice</a>
    <a href="Ribice.php">Ribice</a>
    <a href="Kontakt.php">Kontakt</a>
    <a href="Kosarica.php"><i class="fa fa-shopping-cart fa-lg"></i></a>
    <a href="Login.php" class="active"><i class="fa fa-user fa-lg"></i>
    <a href="javascript:void(0);" class="icon" onclick="DropdownMenu()">
    <i class="fa fa-bars fa-lg"></i>
  </a>
</div>

<div class="content loginregister">
    <br>
    <p>E-mail:</p>
    <input type="email" name="Email" id="Email" value=""><br>
    <p>Zaporka:</p>
    <input type="password" name="Password" id="Password" value=""><br>
    <br>
    <button onclick="Login()">Prijava</button>
    <br>
    <br>
    <button onclick="window.location.href='Register.php'">Registracija</button>
    <p id="result"></p>
</div>

<?php
if(isset($_COOKIE["LoggedIn"])) {
    header("Location: Logout.php", true, 301);
    exit();
} 
?>

</body>

</html>